#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
    int i;
    double result;

    if (argc < 2)
    {
        return 1;
    }

    // 打开生成的文件
    FILE *fout = fopen(argv[1], "w");
    if (!fout)
    {
        return 1;
    }

    // 创建一个开平方根表
    fprintf(fout, "double sqrtTable[] = {\n");
    for (i = 0; i < 10; ++i)
    {
        result = sqrt(i);
        fprintf(fout, "%g,\n", result);
    }

    fprintf(fout, "0};\n");
    fclose(fout);
    return 0;
}
