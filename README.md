CMake-Demo
=====

Demo1:单个源文件  
Demo2:同一目录，多个源文件  
Demo3:多个目录，多个源文件  
Demo4:添加一个版本号和配置的头文件  
Demo5:安装、测试、系统自检  
Demo6:添加自定义命令生成文件  
Demo7:生成安装包  
Demo8:交叉编译  
Demo9:基础语法  

[CMake 入门实战]的源代码。  
