# 生成制作表程序
add_executable(MakeTable MakeTable.c)
target_link_libraries (MakeTable m)
 
# 添加自定义命令产生表
add_custom_command (
  OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/Table.h
  COMMAND MakeTable ${CMAKE_CURRENT_SOURCE_DIR}/Table.h
  DEPENDS MakeTable
  )

# 将给定目录添加到编译器用于搜索包含文件的目录中,这样就能找到Table.h
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )

# 查找当前目录下的所有源文件，并将名称保存到 DIR_LIB_SRCS 变量
aux_source_directory(. DIR_LIB_SRCS)

# 指定生成 MathFunctions 链接库
add_library (MathFunctions ${DIR_LIB_SRCS} ${CMAKE_CURRENT_SOURCE_DIR}/Table.h)

# 指定 MathFunctions 库的安装路径
install (TARGETS MathFunctions DESTINATION lib)
install (FILES MathFunctions.h DESTINATION include)
